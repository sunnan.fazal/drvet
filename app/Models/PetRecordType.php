<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PetRecordType extends Model
{
    protected $fillable = ['record_type'];
}
