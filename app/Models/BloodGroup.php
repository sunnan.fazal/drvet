<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BloodGroup extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['blood_group'];

}
