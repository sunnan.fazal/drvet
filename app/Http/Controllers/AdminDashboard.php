<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\AdminDataTrait;
use App\Http\Controllers\Traits\AdminViewTrait;
use Illuminate\Http\Request;

class AdminDashboard extends Controller
{
    use AdminViewTrait, AdminDataTrait;

}
