<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\DoctorViewTrait;
use App\Http\Controllers\Traits\DoctorDataTrait;
use App\Http\Controllers\Traits\AdminDataTrait;
use Illuminate\Http\Request;

class DoctorDashboard extends Controller
{
	use DoctorViewTrait, DoctorDataTrait, AdminDataTrait;
}
