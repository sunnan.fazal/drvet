<?php

namespace App\Http\Controllers\Traits;

trait DoctorViewTrait
{
	
	public function dashboard()
    {
        return view("doctor.dashboard");
    }

    public function showPets()
    {
    	return view("doctor.pets");
    }	

    public function createpets()
    {
    	return view("doctor.create-pets");
    }	
    
}


?>