<?php

namespace App\Http\Controllers\Traits;

trait AdminViewTrait
{
    public function dashboard()
    {
        return view("admin.dashboard");
    }

    public function showBloodGroups()
    {
        $bloodGroup = $this->get_bloodGroups();
        return view("admin.blood-groups" , compact('bloodGroup'));
    }

    public function addBloodGroup()
    {
        return view("admin.create-blood-group");
    }

    public function petRecordType()
    {
        $petRecord = $this->get_PetRecordType();
        return view("admin.pet-record-type" , compact('petRecord'));
    }
    public function newPetRecordType()
    {
        return view("admin.new-pet-record-type");
    }

    public function petTypes()
    {
        $petType = $this->get_PetTypes();
        return view("admin.pet-type" , compact('petType'));
    }

    public function createPetType()
    {
        return view("admin.create-pet-type");
    }
}
