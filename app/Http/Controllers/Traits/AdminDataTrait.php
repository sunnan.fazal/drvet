<?php

namespace App\Http\Controllers\Traits;

use App\Models\BloodGroup;
use App\Models\PetRecordType;
use App\Models\PetType;
use Illuminate\Http\Request;

trait AdminDataTrait
{
    public function get_bloodGroups()
    {
        return BloodGroup::all();
    }
    public function get_PetRecordType()
    {
        return PetRecordType::all();
    }
    public function get_PetTypes()
    {
        return PetType::all();
    }

    public function saveBloodGroup(Request $request)
    {
        $obj = new BloodGroup();
        $obj->blood_group = $request->get('blood_group');
        $obj->save();

        return redirect()->route('blood-group');
    }

    public function savePetRecordType(Request $request)
    {
        $obj = new PetRecordType();
        $obj->record_type = $request->get('pet_record_type');
        $obj->save();

        return redirect()->route('pet-record-type');
    }

    public function savePetType(Request $request)
    {
        $obj = new PetType();
        $obj->pet_type = $request->get('pet_type');
        $obj->save();

        return redirect()->route('pet-types');
    }
}
