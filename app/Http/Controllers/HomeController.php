<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(Auth::user()->type == Config::get('constants.types.ADMIN'))
        {
            return redirect()->route('admin-dashboard');
        }
        if(Auth::user()->type == Config::get('constants.types.DOCTOR'))
        {
            return redirect()->route('doctor-dashboard');
        }
        if(Auth::user()->type == Config::get('constants.types.USER'))
        {
            return redirect()->route('user-dashboard');
        }

    }
}
