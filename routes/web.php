<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/' , 'HomeController@index');
Route::get('/index' , 'HomeController@index');

Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/', 'AdminDashboard@dashboard')->name('admin-dashboard');
        Route::get('/all-blood-groups' , 'AdminDashboard@showBloodGroups')->name('blood-group');
        Route::get('/add-blood-groups' , 'AdminDashboard@addBloodGroup')->name('new-blood-group');
        Route::post('/save-blood-groups' , 'AdminDashboard@saveBloodGroup')->name('save-blood-group');
        Route::get('/pet-record-type' , 'AdminDashboard@petRecordType')->name('pet-record-type');
        Route::get('/add-pet-record-type' , 'AdminDashboard@newPetRecordType')->name('new-pet-record-type');
        Route::post('/save-pet-record-type' , 'AdminDashboard@savePetRecordType')->name('save-pet-record-type');
        Route::get('/pet-types' , 'AdminDashboard@petTypes')->name('pet-types');
        Route::get('/new-pet-type' , 'AdminDashboard@createPetType')->name('new-pet-type');
        Route::post('/save-pet-type' , 'AdminDashboard@savePetType')->name('save-pet-type');
    });
    Route::group(['prefix' => 'doctor'], function () {
        Route::get('/', 'DoctorDashboard@dashboard')->name('doctor-dashboard');
        Route::get('/all-pets', 'DoctorDashboard@showPets')->name('pets');
        Route::get('/new-pet', 'DoctorDashboard@createpets')->name('new-pets');
    });
    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'UserDashboard@dashboard')->name('user-dashboard');
    });
});



