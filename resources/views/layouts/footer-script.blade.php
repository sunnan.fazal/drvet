        <!-- JAVASCRIPT -->
        <script src="{{ URL::asset('public/assets/libs/jquery/jquery.min.js')}}"></script>
        <script src="{{ URL::asset('public/assets/libs/bootstrap/bootstrap.min.js')}}"></script>
        <script src="{{ URL::asset('public/assets/libs/metismenu/metismenu.min.js')}}"></script>
        <script src="{{ URL::asset('public/assets/libs/simplebar/simplebar.min.js')}}"></script>
        <script src="{{ URL::asset('public/assets/libs/node-waves/node-waves.min.js')}}"></script>

        @yield('script')

        <!-- App js -->
        <script src="{{ URL::asset('public/assets/js/app.min.js')}}"></script>

        @yield('script-bottom')
