@if(Auth::user()->type == \Illuminate\Support\Facades\Config::get('constants.types.ADMIN'))
    @include('admin.sidebar')
@endif
@if(Auth::user()->type == \Illuminate\Support\Facades\Config::get('constants.types.DOCTOR'))
    @include('doctor.sidebar')
@endif
@if(Auth::user()->type == \Illuminate\Support\Facades\Config::get('constants.types.USER'))
    @include('user.sidebar')
@endif

<!-- Left Sidebar End -->
