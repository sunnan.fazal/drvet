@extends('layouts.master')

@section('title') All Pets @endsection


@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('public/assets/libs/datatables/datatables.min.css')}}">
@endsection

@section('content')


    @component('common-components.breadcrumb')
        @slot('title') All Pets  @endslot
        @slot('li_1') <a href="{{ route('new-pets') }}" style="float: right" type="button" class="btn btn-outline-primary waves-effect waves-light">Create Pet</a> @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="card-title">Pets Listing</h4>
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end col -->
    </div>

@endsection

@section('script')

    <!-- Plugins js -->
    <script src="{{ URL::asset('public/assets/libs/datatables/datatables.min.js')}}"></script>
    <!-- Init js-->
    <script src="{{ URL::asset('public/assets/js/pages/datatables.init.js')}}"></script>

@endsection
