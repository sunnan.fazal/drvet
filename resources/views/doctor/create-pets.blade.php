@extends('layouts.master')

@section('title') Creating Pet @endsection


@section('css')

@endsection

@section('content')

    <form class="needs-validation" novalidate="" action="" method="GET">
        @csrf
    @component('common-components.breadcrumb')
        @slot('title') Create new Pet  @endslot
        @slot('li_1') <button type="button"  style="float: right" type="button" class="btn btn-outline-primary waves-effect waves-light">Save Pet</button> @endslot
    @endcomponent

    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Basic Information</h4>
                    <br>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group position-relative">
                                    <label for="validationTooltip01">Select Type</label>
                                    <select class="form-control" name="pet_type"></select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group position-relative">
                                    <label for="validationTooltip01">Select Vet</label>
                                    <select class="form-control" name="vet"></select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group position-relative">
                                    <label for="validationTooltip01">Name</label>
                                    <input type="text" name="name" class="form-control"> 
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group position-relative">
                                    <label for="validationTooltip01">Gender</label>
                                    <select class="form-control" name="gender">
                                        <option value="MALE">Male</option>
                                        <option value="FEMALE">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group position-relative">
                                    <label for="validationTooltip01">Age</label>
                                    <input type="text" name="age" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group position-relative">
                                    <label for="validationTooltip01">Sterilized</label>
                                    <select class="form-control" name="sterilized">
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group position-relative">
                                    <label for="validationTooltip01">Insured</label>
                                    <select class="form-control" name="insured">
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group position-relative">
                                    <label for="validationTooltip01">Microchip</label>
                                    <input type="text" name="microchip" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group position-relative">
                                    <label for="validationTooltip01">Blood Group</label>
                                    <select class="form-control" name="bloodgroup"></select>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <!-- end card -->
        </div> <!-- end col -->
    </div>
    </form>
@endsection

@section('script')



@endsection
