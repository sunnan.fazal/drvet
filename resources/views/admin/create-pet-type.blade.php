@extends('layouts.master')

@section('title') Pet Type Setup @endsection


@section('css')

@endsection

@section('content')

    <form class="needs-validation" novalidate="" action="{{ route('save-pet-type') }}" method="POST">
        @csrf
        @component('common-components.breadcrumb')
            @slot('title') Pet Type Setup  @endslot
            @slot('li_1') <button type="submit"  style="float: right" type="button" class="btn btn-outline-primary waves-effect waves-light">Save Type</button> @endslot
        @endcomponent

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Create New Pet Type</h4>
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group position-relative">
                                    <label for="validationTooltip01">Pet Type</label>
                                    <input type="text" class="form-control" name="pet_type" placeholder="Enter Pet Type"  required="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end col -->
        </div>
    </form>
@endsection

@section('script')



@endsection
