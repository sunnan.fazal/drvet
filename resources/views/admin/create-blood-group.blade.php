@extends('layouts.master')

@section('title') Blood Group Setup @endsection


@section('css')

@endsection

@section('content')

    <form class="needs-validation" novalidate="" action="{{ route('save-blood-group') }}" method="POST">
        @csrf
    @component('common-components.breadcrumb')
        @slot('title') Blood Group Setup  @endslot
        @slot('li_1') <button type="submit"  style="float: right" type="button" class="btn btn-outline-primary waves-effect waves-light">Save Blood Group</button> @endslot
    @endcomponent

    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Create New Blood Group</h4>
                    <br>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group position-relative">
                                    <label for="validationTooltip01">Blood Group</label>
                                    <input type="text" class="form-control" name="blood_group" placeholder="Enter Blood group"  required="">
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <!-- end card -->
        </div> <!-- end col -->
    </div>
    </form>
@endsection

@section('script')



@endsection
