@extends('layouts.master')

@section('title') Pet Record Type Setup @endsection


@section('css')

@endsection

@section('content')

    <form class="needs-validation" novalidate="" action="{{ route('save-pet-record-type') }}" method="POST">
        @csrf
        @component('common-components.breadcrumb')
            @slot('title') Pet Record Type Setup  @endslot
            @slot('li_1') <button type="submit"  style="float: right" type="button" class="btn btn-outline-primary waves-effect waves-light">Save Type</button> @endslot
        @endcomponent

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Create New Pet Record Type</h4>
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group position-relative">
                                    <label for="validationTooltip01">Type</label>
                                    <input type="text" class="form-control" name="pet_record_type" placeholder="Enter Record Type"  required="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card -->
            </div> <!-- end col -->
        </div>
    </form>
@endsection

@section('script')



@endsection
