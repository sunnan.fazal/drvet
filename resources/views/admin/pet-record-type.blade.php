@extends('layouts.master')

@section('title') Pet Record Type Setup @endsection


@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('public/assets/libs/datatables/datatables.min.css')}}">
@endsection

@section('content')


    @component('common-components.breadcrumb')
        @slot('title') Pet Record Type Setup  @endslot
        @slot('li_1') <a href="{{ route('new-pet-record-type') }}" style="float: right" type="button" class="btn btn-outline-primary waves-effect waves-light">Create Pet Record Type</a> @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="card-title">All Pet Record Types</h4>
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>Pet Record Type</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($petRecord as $pr)
                                <tr>
                                    <td>{{ $pr->record_type }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end col -->
    </div>

@endsection

@section('script')

    <!-- Plugins js -->
    <script src="{{ URL::asset('public/assets/libs/datatables/datatables.min.js')}}"></script>
    <!-- Init js-->
    <script src="{{ URL::asset('public/assets/js/pages/datatables.init.js')}}"></script>

@endsection
