<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="javascript: void(0);" class="waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span>Dashboards</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{ url('/') }}">Default</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-layout"></i>
                        <span>Administration</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="#">Users</a></li>
                        <li><a href="{{ route('pet-types') }}">Pet Types</a></li>
                        <li><a href="{{ route('pet-record-type') }}">Pet Record Types</a></li>
                        <li><a href="#">Pet Records</a></li>
                        <li><a href="{{ route('blood-group') }}">Blood Group</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
